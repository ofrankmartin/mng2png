#include "converter.h"
#include <QUrl>
#include <QTime>
#include <QDebug>

#include "utils.h"

Converter::Converter(QObject *parent) :
    QObject(parent) {
}

int Converter::convertMNG2PNG(const QString &originFileName, const QString &prefix, const QString &destDir) {
    qDebug() << QTime::currentTime().toString();
    QFile file(originFileName);
    file.open(QIODevice::ReadOnly);

    QString usePrefix = prefix;
    if (prefix.isNull()) {
        usePrefix = Utils::extractPreffix(originFileName);
    }

    QDir useDir;
    if (destDir.isNull()) {
        useDir = Utils::extractDir(originFileName);
    } else {
        useDir.setPath(destDir);
    }

    QByteArray pngSignature("\x89\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00\x0d", 12);
    unsigned fileIndex = 0;
    QByteArray buffer;
    while (!file.atEnd()) {
        buffer.append(file.read(1));
        buffer = buffer.right(4);
        bool pngBegin = Utils::compareQByteArray(buffer, QByteArray("IHDR"));
        if (pngBegin) {
            bool pngEnd = false;
            QByteArray fileContent(pngSignature);
            fileContent.append("IHDR");
            buffer.clear();
            while (!pngEnd && !file.atEnd()) {
                fileContent.append(file.read(1));
                QByteArray fileTail(fileContent.right(4));
                pngEnd = Utils::compareQByteArray(fileTail, QByteArray("IEND"));

                if (pngEnd) {
                    QString suffix = QString("_%1").arg((uint)fileIndex++, 2, 10, QChar('0'));
                    QString fileName = useDir.absoluteFilePath(usePrefix + suffix + ".png");
                    QFile outPng(fileName);
                    outPng.open(QIODevice::WriteOnly);
                    outPng.write(fileContent);
                    outPng.close();
                }
            }
        }
    }
    file.close();
    qDebug() << QTime::currentTime().toString();

    return fileIndex;
}
