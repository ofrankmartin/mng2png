#-------------------------------------------------
#
# Project created by QtCreator 2015-08-31T21:45:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MNG2PNG
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    utils.cpp \
    converter.cpp

HEADERS  += mainwindow.h \
    utils.h \
    converter.h

FORMS    += mainwindow.ui
