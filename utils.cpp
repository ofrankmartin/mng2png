#include "utils.h"
#include <QUrl>

Utils::Utils(QObject *parent) :
    QObject(parent) {
}

bool Utils::compareQByteArray(const QByteArray &a1, const QByteArray &a2) {
    if (a1.size() != a2.size()) {
        return false;
    }

    for (unsigned i = 0; i < a1.size(); i++) {
        if (a1.at(i) != a2.at(i)) {
            return false;
        }
    }

    return true;
}

QString Utils::extractPreffix(const QString &fileName) {
    QUrl fileUrl(fileName);
    QString prefix = fileUrl.fileName();
    prefix = prefix.left(prefix.size() - 4);

    return prefix;
}

QDir Utils::extractDir(const QString &fileName) {
    QUrl fileUrl(fileName);
    QDir fileDir(fileUrl.adjusted(QUrl::RemoveFilename).toString());

    return fileDir;
}
