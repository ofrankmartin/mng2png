#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QDir>

class Utils : public QObject
{
    Q_OBJECT
public:
    explicit Utils(QObject *parent = 0);
    static bool compareQByteArray(const QByteArray& a1, const QByteArray& a2);
    static QString extractPreffix(const QString &fileName);
    static QDir extractDir(const QString &fileName);
signals:

public slots:

};

#endif // UTILS_H
