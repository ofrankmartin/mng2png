#ifndef CONVERTER_H
#define CONVERTER_H

#include <QObject>
#include <QDir>

class Converter : public QObject
{
    Q_OBJECT
public:
    explicit Converter(QObject *parent = 0);

signals:

public slots:
    int convertMNG2PNG(const QString& originFileName, const QString& prefix = QString(), const QString &destDir = QString());
};

#endif // CONVERTER_H
