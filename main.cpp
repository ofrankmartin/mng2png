#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGuiApplication::setOrganizationName("JLMGames");
    QGuiApplication::setOrganizationDomain("jlmgames.com");
    QGuiApplication::setApplicationName("MNG2PNG");
    MainWindow w;
    w.show();

    return a.exec();
}
