#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFile>
#include <QDebug>

#include "converter.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_browseBtn_clicked() {

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("MNG Files (*.mng)"));
    if (!fileName.isEmpty()) {
        ui->fileNameEdit->setText(fileName);
    }
}

void MainWindow::on_convertBtn_clicked() {
    Converter converter;
    qDebug() << converter.convertMNG2PNG(ui->fileNameEdit->text());
}


